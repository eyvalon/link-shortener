## Description

A small project made using React Framework integrating the use of Material UI. This app aim to transform given link to a shorter form (which potentially could be used to serve for sharing links to others), allowing it to be encapsulated as small as `$URL/UNIQUE_CODE` instead of being a long URL (i.e. `www.example.com/blog/page/x`).

### Images

![Alt Text](./images/linkShortener.gif)

### Features:

- Using Local Storage to store list of URL (in replacement of RESTful API to grab and redirect to external page from Back-End)
- Correct routing page (home, 404 and slug) and forwarding the short link to the actual link

**NOTES**

- Since the project is using Local Storage to parse in JSON, it is possible to adjust to storing to actual database server by creating a Back-End with Database integration (i.e. using NodeJs with MongoDB)

## Usage

### Installing dependencies

```bash
$ npm install
```

### Running project

By default, the project uses port 3000

```bash
$ npm run start
```

### Building for production

To build the app, which will generate a static files (in the `build` folder), we need to run the following command:

```bash
$ npm run build
```
