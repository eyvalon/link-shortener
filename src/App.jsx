// Defining imports
import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
// import { Redirect } from "react-router-dom";

import { withStyles } from "@material-ui/styles";
import PropTypes from "prop-types";

// Importing components
import Home from "./components/Home";
import Error from "./components/Error";
import Header from "./components/Header";
import SlugPage from "./components/Slug";

// Creating an absolute area to bind to the border
const styles = {
  root: {},
  MainContainer: {
    marginTop: "2em"
  },
  Wrapper: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0
  }
};

class App extends Component {
  // constructor(props) {
  //   super(props);
  // }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.Wrapper}>
        <Header />
        <div className={classes.MainContainer}>
          {/* Defining route structures for this app */}
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/404" component={Error} />
            <Route path="/:slug" component={SlugPage} />
            {/* <Redirect to="/404" /> */}
          </Switch>
        </div>
      </div>
    );
  }
}

// Material UI's version of overriding existing styling with custom CSS
App.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(App);
