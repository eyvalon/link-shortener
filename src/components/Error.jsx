// Defining imports
import React, { Component } from "react";

// Defining how error page should look like
class Error extends Component {
  render() {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh"
        }}
      >
        <h1>This page does not exist</h1>
      </div>
    );
  }
}

export default Error;
