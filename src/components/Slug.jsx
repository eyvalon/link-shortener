// Defining imports
import React, { Component } from "react";

class SlugPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      links: [],
      currentRedirect: ""
    };

    this.redirectLink = this.redirectLink.bind(this);
  }

  // Get data from Local Storage and then store to state links
  onGetValue() {
    const cachedHits = localStorage.getItem("links");
    if (cachedHits) {
      this.setState({ links: JSON.parse(cachedHits) });
    }
  }

  // For matched result, we first check to see if the input URL is valid in the form of
  // https OR http, if not, we put https in front to allow window.location to work correctly
  redirectLink(slug) {
    let allLinks = this.state.links;
    let currentLink = "";

    allLinks.filter(function (item) {
      if (item.id === slug) {
        if (
          !item.link.includes("localhost") &&
          !item.link.includes("https") &&
          !item.link.includes("http")
        ) {
          currentLink = `https://${item.link}`;
        } else {
          currentLink = `${item.link}`;
        }
        return true;
      }
      return false;
    });

    return currentLink;
  }

  // As the component loads, get data from Local Storage (if exists)
  async componentDidMount() {
    const cachedHits = localStorage.getItem("links");
    const { slug } = this.props.match.params;

    // If a data exists in Local Storage, first check to see if there is a match
    if (cachedHits) {
      let result = JSON.parse(cachedHits);
      this.setState({ links: result }, () => {
        this.currentRedirect = this.redirectLink(slug);

        // Redirect user according to if results were found
        if (this.currentRedirect === "") {
          window.location = "/404";
        } else {
          window.location = this.currentRedirect;
        }
      });
    }
  }

  render() {
    let isLink = this.currentRedirect;

    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh"
        }}
      >
        {isLink ? (
          <p>
            If the page doesn't automatically redirect you, you can click{" "}
            <a href={`${this.currentRedirect}`}>here</a>
          </p>
        ) : (
          <p>Redirecting...</p>
        )}
      </div>
    );
  }
}

export default SlugPage;
