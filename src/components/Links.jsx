// Defining imports
import React, { Component } from "react";
import { Link } from "react-router-dom";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import DeleteIcon from "@material-ui/icons/Delete";

import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";

// Defining custom color scheme for table columns
const styles = {
  trashIcon: {
    "&:hover": { color: "#ba000d" }
  },
  columnId: {
    color: "black;",
    fontWeight: "bold"
  },
  columnLink: {
    color: "#d81b60",
    wordBreak: "break-word"
  },
  columnShortLink: {
    textDecoration: "none",
    color: "blue;",
    "&:visited": { color: "#d81b60" },
    "&:hover": { textDecoration: "underline", color: "blue" }
  },
  rowSelector: {
    "&:hover": { background: "#f5f5f5" }
  }
};

// Defining styles for table layout
const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14
  }
}))(TableCell);

// Defining styles for table rows
const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover
    }
  }
}))(TableRow);

class Links extends Component {
  constructor(props) {
    super(props);
    this.tableHeader = [{ id: 0, link: "", short_link: 21, action: "" }];
  }

  render() {
    const { classes } = this.props;

    return (
      <TableContainer>
        <Table aria-label="customized table">
          <TableHead>
            <TableRow>
              {/* {this.renderTableHeader()} */}
              <StyledTableCell width="20%">
                {"id".toUpperCase()}
              </StyledTableCell>
              <StyledTableCell width="40%">
                {"link".toUpperCase()}
              </StyledTableCell>
              <StyledTableCell width="15%">
                {"short_link".toUpperCase()}
              </StyledTableCell>
              <StyledTableCell width="15%">
                {"action".toUpperCase()}
              </StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.props.links.map((row) => (
              <StyledTableRow key={row.id} className={classes.rowSelector}>
                <StyledTableCell className={classes.columnId} scope="row">
                  {row.id}
                </StyledTableCell>
                <StyledTableCell className={classes.columnLink}>
                  {row.link}
                </StyledTableCell>
                <StyledTableCell>
                  <Link to={`/${row.id}`} className={classes.columnShortLink}>
                    {row.short_link}
                  </Link>
                </StyledTableCell>
                <StyledTableCell>
                  <DeleteIcon
                    className={classes.trashIcon}
                    onClick={() => this.props.onDelete(row.id)}
                  />
                </StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

// Material UI's version of overriding existing styling with custom CSS
Links.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Links);
