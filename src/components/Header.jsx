// Defining imports
import React, { Component } from "react";

import AppBar from "@material-ui/core/AppBar";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";

import { withStyles } from "@material-ui/styles";
import PropTypes from "prop-types";

// Creating an absolute area to bind to the border
const styles = {
  root: {
    maxWidth: "900px;"
  }
};

class Header extends Component {
  render() {
    const { classes } = this.props;

    return (
      <AppBar position="static" color="primary">
        <div className={classes.root}>
          <Grid justify="space-between" container>
            <div>
              <h2 style={{ padding: "0 0.6em", cursor: "pointer" }}>
                <Link to="/" style={{ textDecoration: "none", color: "white" }}>
                  Link Shortener{" "}
                </Link>
              </h2>
            </div>
          </Grid>
        </div>
      </AppBar>
    );
  }
}

// Material UI's version of overriding existing styling with custom CSS
Header.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Header);
