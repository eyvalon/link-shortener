// Defining imports
import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { Alert } from "@material-ui/lab";

import PropTypes from "prop-types";
import { withStyles } from "@material-ui/styles";

// Importing components
import Links from "./Links";

// Defining layout for the Home Page
const styles = {
  root: {
    flexGrow: 1,
    width: "900px",
    margin: "0 auto"
  },
  LinksContainer: {
    marginTop: "2em"
  },
  buttons: {
    margin: "auto"
  }
};

class Home extends Component {
  constructor(props) {
    super(props);
    this.title = "Home";
    this.state = {
      value: "",
      links: []
    };
    this.error = false;
    this.errorMessage = "";
    this.show = false;

    // Helps to bind functions on load
    this.buttonPressed = this.buttonPressed.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  // Save data to Local Storage (helping to retrieve existing data upon page reloads)
  onSaveValue() {
    localStorage.setItem("links", JSON.stringify(this.state.links));
  }

  // A function that helps remove item from the list
  onDelete(id) {
    let newList = this.state.links.filter((item) => item.id !== id);
    this.setState({ links: newList });

    // Manually setting new links list
    localStorage.setItem("links", JSON.stringify(newList));
  }

  // As the component is loaded, check to see if any values are stored in Local Storage
  componentDidMount() {
    this.onGetValue();
  }

  // Get data from Local Storage and then store to state links
  onGetValue() {
    const cachedHits = localStorage.getItem("links");
    if (cachedHits) {
      this.setState({ links: JSON.parse(cachedHits) });
    }
  }

  // A URL checker based off (https://stackoverflow.com/questions/5717093/check-if-a-javascript-string-is-a-url)
  URLChecker(str) {
    var pattern = new RegExp(
      "^(https?:\\/\\/)?" + // protocol
      "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
      "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
      "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
      "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
        "(\\#[-a-z\\d_]*)?$",
      "i"
    ); // fragment locator
    return !!pattern.test(str);
  }

  // Check validity of link
  checkLink() {
    let valueList = this.state.value;
    let found = false;

    if (!this.URLChecker(valueList) || valueList === null) {
      this.error = true;
      this.errorMessage = "Invalid URL";
    } else {
      this.error = false;
      this.errorMessage = "URL is valid";
    }

    // Checks for duplication of links
    this.state.links.filter(function (item) {
      if (item.link === valueList) {
        return (found = true);
      }
      return false;
    });

    if (found === true) {
      this.error = true;
      this.errorMessage = "Duplicated links found";
    }
  }

  // Listen for change in link value
  onChangeValue = (event) => {
    if (this.show) {
      this.checkLink();
    }

    this.setState({ value: event.target.value });
  };

  // Generating a random unique keys (similar to nanoid plugin)
  generateID() {
    return Math.random().toString(36).substr(2, 9);
  }

  // A function that checks for the validity of input URL
  addToList() {
    this.show = true;
    this.checkLink();

    if (!this.error) {
      this.object = this.generateObject();
      this.state.links.push(this.object);

      this.onSaveValue();
      this.setState({ value: "" });
    }
  }

  // Generate Object list for short links (which are valid)
  generateObject() {
    let generatedId = this.generateID();

    let object = {
      id: generatedId,
      link: this.state.value,
      short_link: window.location.hostname + "/" + generatedId,
      action: ""
    };

    return object;
  }

  // A function that handles button pressing on "ADD"
  buttonPressed(e) {
    e.preventDefault();

    this.show = true;
    this.addToList();
  }

  // A function that handles key pressing for "Enter"
  handleKeyDown = (event) => {
    if (event.key === "Enter") {
      this.addToList();
    }
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        {this.show ? (
          <Alert variant="outlined" severity={this.error ? "error" : "success"}>
            {this.errorMessage}
          </Alert>
        ) : (
          <div></div>
        )}

        <Grid container spacing={3} className={classes.LinksContainer}>
          <Grid item xs={12} sm={9}>
            <TextField
              label="Enter URL to shorten"
              id="standard-full-width"
              color="primary"
              fullWidth
              value={this.state.value}
              onChange={this.onChangeValue}
              onKeyDown={this.handleKeyDown}
            />
          </Grid>
          <Grid item xs={12} sm={3} className={classes.buttons}>
            <Button
              variant="contained"
              color="primary"
              type="button"
              fullWidth
              xs={12}
              sm={6}
              onClick={this.buttonPressed}
              disabled={!this.state.value}
            >
              Add
            </Button>
          </Grid>
        </Grid>

        <div className={classes.LinksContainer}>
          <Links links={this.state.links} onDelete={this.onDelete} />
        </div>
      </div>
    );
  }
}

// Material UI's version of overriding existing styling with custom CSS
Home.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Home);
